<?php
/**
 * @file
 * Contains \Drupal\widget_block\Form\WidgetBlockConfigDeleteForm.
 */

namespace Drupal\widget_block\Form;

use Drupal\Core\Entity\EntityDeleteForm;

/**
 * Provides a configuration form for deleting a widget block configuration entity.
 */
class WidgetBlockConfigDeleteForm extends EntityDeleteForm { }
